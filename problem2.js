const problem2 = inventory => {

  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  const lastCar = inventory.pop();
  return console.log(`Last car is a  ${lastCar.car_make} ${lastCar.car_model}`);
};

module.exports = problem2;
