const vows = require('vows');
const assert = require('assert');
const util = require('util');
util.print = console.log;
util.puts = console.log;

const inventory = require('../public/src/inventory');
const problem2 = require('../problem2');

vows
  .describe('finding the last car function')
  .addBatch({
    'when inventory is not an array': {
      topic: () => {
        try {
          problem2({}, 33);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Inventory is not an Array of Cars');
      },
    },
    'the last car of inventory': {
      topic: problem2(inventory),
      'should not throw an error': result => {
        assert.doesNotThrow(() => result);
      },
    },
  })
  .run();
