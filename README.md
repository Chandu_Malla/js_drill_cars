# JS Drill: Cars

## Description

To help us use arrays with real world problems we are going to simulate a used car dealer that has 50 cars in their inventory. The car dealer has all of their inventory housed in the array seen below. Scroll down past the data to find out how you can help the car dealer.

PROJECT RESTRICTION: You can't use map, reduce, or filter to solve these problems. Only use native JavaScript for loops. No other types of loops are allowed.

Create a function for each problem in a file called

    problem1.js
    problem2.js
    problem3.js
and so on in the root of the project.

Ensure that the functions in each file is exported and tested in its own file called

    testProblem1.js
    testProblem2.js
    testProblem3.js
and so on in a folder called `test`.

Each function must take at least one argument. The first argument must always be the inventory.

Example:
```js
const result = problem1(inventory);
```

If you want to pass more arguments, that is up to you.

Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project.
Ensure that the repo is a public repo.

// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"

// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.



## Table of Contents

- [JS Drill: Cars](#js-drill-cars)
  - [Description](#description)
  - [Table of Contents](#table-of-contents)
  - [Use NVM](#Use NVM)
  - [Installation](#installation)
- [Navigate to the project directory](#navigate-to-the-project-directory)
- [Install dependencies](#install-dependencies)
    - [Testing](#testing)
  - [Testing the solutions](#testing-the-solutions)
   

## Installation

Describe the steps to install the project and its dependencies. If there are specific requirements or configurations, mention them here.

```bash
# Clone the repository
git clone git@gitlab.com:Chandu_Malla/js_drill_cars.git
```

## Use NVM 

Change node version to 14
```
nvm install 14.17.6
nvm alias default 14.17.6
```

# Navigate to the project directory
```
cd js_drill_cars
```

# Install dependencies
```
npm install
```

### Testing

Each test file in the test folder tested indivdually with custom test 
cases.

## Testing the solutions

Go through the test directory then run node command on each test

```
cd test/
```

By running the below command, which executes custom test cases & returns output

```
node testProblem1.js
```
