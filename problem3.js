const problem3 = inventory => {

  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  const car_models = new Array();

  for (const data of inventory) {
    car_models.push(data.car_model);
  }

  return console.log(car_models.sort());
};

module.exports = problem3; 
