const problem1 = (inventory, id) => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  } else if (isNaN(id)) {
    throw new Error('Id is not a valid number');
  } else if (id > 50) {
    throw new Error('Id range exceeds the Inventory length');
  }

  for (const data of inventory) {
    if (data.id === id) {
      return console.log(
        `Car ${id} is a ${data.car_year} ${data.car_make} ${data.car_model}`,
      );
    }
  }
};

module.exports = problem1;
