const problem6 = inventory => {
    
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  const bmwAndaudiCars = new Array();

  inventory.forEach(data => {
    if (data.car_make == 'Audi' || data.car_make === 'BMW') {
      bmwAndaudiCars.push(data);
    }
  });

  return console.log(
    `List of BMW and AUDI cars is: ${JSON.stringify(bmwAndaudiCars, '', 1)}`,
  );
};

module.exports = problem6;
