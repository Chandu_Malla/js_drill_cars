const problem4 = inventory => {

  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  const car_year_list = new Array();

  inventory.forEach(data => car_year_list.push(data.car_year));

  return car_year_list;
};

module.exports = problem4; 
