const problem5 = (inventory, listOfCarYears) => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  } else if (listOfCarYears.length == 0 || !Array.isArray(listOfCarYears)) {
    throw Error('List of car years is not found');
  }

  let cars_before_2000 = 0;
  let listOfOlderCars = new Array();

  listOfCarYears.forEach(year => {
    cars_before_2000 += year < 2000 ? 1 : 0;
  });

  inventory.forEach(data => {
    if (data.car_year > 2000) {
      listOfOlderCars.push(data.car_make);
    }
  });

  console.log(
    `Cars made before the year 2000: ${cars_before_2000}, array of older cars: ${listOfOlderCars}, total older cars: ${listOfOlderCars.length}`,
  );
};

module.exports = problem5;
